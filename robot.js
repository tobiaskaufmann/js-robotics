let robot = require("robotjs");
let fs = require("fs");
let Jimp = require('jimp')
let cv = require('opencv');

// Speed up the mouse.
robot.setMouseDelay(2);

let twoPI = Math.PI * 2.0;
let screenSize = robot.getScreenSize();
let height = (screenSize.height / 2) - 10;
let width = screenSize.width;

for (let x = 0; x < width; x++)
{
	y = height * Math.sin((twoPI * x) / width) + height;
	robot.moveMouse(x, y);
}

let sizeX = 1920;
let sizeY = 1080;
let rimg = robot.screen.capture(0, 0, sizeX, sizeY);
let path = 'myfile.png';


// var jimg = new Jimp(sizeX, sizeY);
// for (var x=0; x<sizeX; x++) {
//     for (var y=0; y<sizeY; y++) {
//         var index = (y * rimg.byteWidth) + (x * rimg.bytesPerPixel);
//         var r = rimg.image[index];
//         var g = rimg.image[index+1];
//         var b = rimg.image[index+2];
//         var num = (r*256) + (g*256*256) + (b*256*256*256) + 255;
//         jimg.setPixelColor(num, x, y);
//     }
// }
// jimg.write(path);



// cv.readImage('./original.PNG', function(err, im) {
//   if (err) return console.error('error loading image');

//   var output = im.matchTemplate('./original.PNG', 3);
//   console.log(output);
//   var matches = output[0].templateMatches(0.80, 1.0, 5, false);

//   console.log(matches);
// });
